啪！
從可變魔法槍中射出的全力一擊，被赫洛斯的大劍──Excalibur輕易地消除了。
轉變的赫洛斯，機體是全身半透明的綠色，大劍也變成了綠色。
一切都成為了奧里哈魯鋼。

「哇～死吧，白詰！」

從赫洛斯的背部釋放出，三根擁有追尾能力的光之箭。
HP沒有那麼方便地恢復好，在還剩下３０００的狀態下，被擊中就死了。
總之我先背向赫洛斯，全力逃跑。
當然，克勞索拉斯的射速更快，我也知道這只是一種無力的掙扎。
即便如此，除了逃跑以外，也沒有想到其他的回避方法。

「啊⋯⋯啊，哇⋯⋯啊啊啊！」

啪嘰，啪嘰，踩倒樹木、踏在土地上，只是一味的一步一步向前跑。
向前，向前，向前。
我從沒有像今天這樣如此恨自己不能飛上天空，為什麼人類沒有翅膀，為什麼不能用克勞鳥索拉斯以上的速度行動？
逼近的三縷光芒，攜帶著死亡朝向了我。
啊，反正要死的──如果此處是以前的我，在這裡就會放棄了吧。
會歡迎死亡的吧。
但是，現在的我有著對於生的執著。
即使知道是徒勞，也必須去掙扎。

「技能發動，羨慕吧，我的領域！」

第三次發動「羨慕吧，我的領域」

用力踢向地面，身體向著天空上升。
當然，克勞烏索拉斯也追了上來。
不能在空中飛的烏璐緹歐，沒有了落腳的地面，運動也會被限制，也就是說在空中無法進行回避運動。
一般考慮的話，這就是自殺行為。
因此，我要追加使用技能，聽天由命的賭一把──

「技能發動，我親愛的朋友！」

烏璐緹歐的姿態，在空中變成了廣瀨的阿尼瑪的樣子。
那道光是以什麼為基準追踪敵人的呢？
這個技能到底能欺騙對方到什麼程度呢？
在與赫洛斯・普拉斯的戰鬥時，克勞鳥索拉斯的三支光矢靈巧地分別追上了三個人。
也就是說，不是自動的追踪敵人，而是可以設定為追踪α，追踪β這個樣子。
那樣的話，改變姿態，讓其無法認識到是烏璐緹歐的話不就可以回避了嗎？

這種對自己稱心如意的解釋，卻賭上了我的生命。

半空中的機體，不抵抗重力開始掉向地面。
瞄準了那樣烏璐緹歐的三支光矢──不追踪下落的我，向著沒有任何東西的虛空筆直的飛了過去。

「哈哈⋯⋯賭對了啊！」

落到地面，我深深地嘟囔著。
那個追尾意外的是以單純的結構而行動著。
雖然不能說封住了，但我已經明白了克勞烏索拉斯的攻略方法。
對於奄奄一息的我，失去了最適合的追尾武裝的現在，赫洛斯留下的是大劍「Excalibur」和大型魔法槍「卡文汀」

雖然這麼說，但那也挺麻煩的。

也許是被避開了克勞烏索拉斯，出乎了赫洛斯的意料，赫洛斯停止了行動。
在那期間，我試著發射了可變魔法槍，但是被Excalibur輕易地切斷攻擊。
並不是發呆，而是在思考下一個手段吧。
然後慢慢地，朝著左邊的山凝視而去。
不，不是山嗎？
仔細看的話，赫洛斯的視線在那稍微上面的天空──看著那些展翅的鳥群。

「共有，使用！」

這樣說著，向著鳥伸出手掌。
手掌上的魔力──不，奧里哈魯鋼的旋渦，一點點地形成結晶。
什麼，想做什麼？
我一邊慢慢地遠離赫洛斯，一邊吞咽著唾沫注視著那個姿態。
當晶體達到某種程度上的大小時，就突然漂浮起來，向著山上的鳥群前進。

一口氣加速，到達鳥群中央的結晶，當場彈起，裂開的碎片擴散了。
過了一會兒，鳥群們發生了變化。
群體全部被光包圍著，變成了一個纏著巨大金屬鎧甲的姿態。

「阿尼瑪化⋯⋯我知道⋯⋯？」

奧里哈魯鋼是使魔力增幅的物質。
阿尼瑪是魔力很高的人類才能夠使用的力量。
那麼同理，攝取了奧里哈魯鋼的生物也能夠使用的吧，應該是這樣的吧。
但是，雖說是野生的魔物，但那個數量──！

「奧里哈魯鋼的美妙之處，共享！」

變成了魔物的鳥群，明顯地接近了過來。

抱持著明確的敵意，不是對赫洛斯，而是烏璐緹歐。
我慌慌張張地解除了魔彈的射手，把可變魔法槍的狙擊模式切換掉。

「殲滅形態！」

如果被魔物群包圍的話，就無法回避赫洛斯的攻擊了，首先擊破它們！
如果是剛誕生的魔物的話，只有Lv.１，能力應該還很低。
那樣的話，就用這一擊──想要扣下扳機的瞬間，盯著鳥群的赫洛斯朝向了這裡。

「消散吧！」

指向我的是卡文汀的槍口。
反射性的後退，我馬上離開了那條射線。
本該是很從容的回避了⋯⋯但是，射出的卡文汀驚險的擦過了烏璐緹歐，穿過了烏璐緹歐之前所在的地方。
明顯比以前更大了，無論是威力上，還是幅度上。
在卡文汀的射線經過之後，剩下的只有一條什麼都沒有的路。
樹木消失，山上開出了一條漂亮的隧道。
如果對面有個城市的話，那裡也會消失的吧。

在回避卡文汀的過程中，魔物群逼近了這裡。
雖然著地之後馬上就釋放了魔法槍，但還是慢了一手。
雖然命中了，但魔物們卻分散開，被捲入爆炸的只有極少數。
在烏璐緹歐周圍圍繞著鳥群的魔物。
對揮舞著銳利的鈎爪靠近過來的傢伙們，用頭部魔法槍一隻一隻地擊落下來。
一旦有了餘裕的話，赫洛斯就用卡文汀射擊，然後被迫要回避卡文汀。

「咕⋯⋯數量太多了⋯⋯！」

與赫洛斯的火力相比，魔物們的攻擊不算很大的威力。
但是，這個數量的魔物不可能全部應對過來，HP確實被一點一點地消減了。

視野邊緣所看到的數字更加地誘發了焦慮。
因此判斷力變得遲鈍，不必要的傷害也增加了。
HP──８４０ ／ ５５８００。
想辦法把鳥群的魔物擊破，但是在那之後還要做赫洛斯的對手的話，有點吃力啊。

在愛克羅賽的居民避難結束了之後，要去尋求幫助嗎？
不，逃跑也是一件辛苦的事情，這點HP只會白白地招致死亡。
那麼，期待援軍吧──啊，不行不行，先不考慮自己做點什麼的話。

「平均的，平等的！」

在鳥群魔物數量減少到一定程度的時候，突然從赫洛斯那邊襲來的攻擊中斷了。
看著魔物的樣子，這次張開了雙手，在手掌上生成奧里哈魯鋼的結晶。
難道說──又要生成魔物了嗎？

「靠近依賴，乞討，被虐待，白詰總是那樣！」
「什⋯⋯」

桂第一次跟我說話。

事出突然，我沒能好好地做出應答。

「我就是出生在這樣的環境之下，我也無法忍受這樣的生活啊。」

把最後的魔物用劍砍掉，面向了桂。

「即使變成那樣的身體，也能好好地說出來啊。」
「剛剛還沒能好好地說出自己想的事情。現在已經習慣了，反而覺得這樣比較好。」

我覺得那樣的綠色身體，一般是會討厭的。

「最下層。白詰，對你來說這句話是最合適的。」
「我先說明，這樣的生活方式我也不是自願的。」

不是只有我，任誰都不想被欺負。

「確實人際關係是從別人那裡得到的。家族、朋友、同班同學、教師、他們形成、與我們糾纏的惡意和善意、利害交織的包圍網。所以不說這是『自我責任』，人際關係的形成有很大的運氣成分，值得同情的地方也有很多吧。但是老實說，我看不起你。」

我知道了。
總覺得確實是那樣的想法。
桂偉月這個人成績優秀，運動神經超群，而且是優等生。
不僅僅是外表，連性格都很完美，即使如此也沒有幫助我。
像我一開始就不在那裡一樣。
不過，注意到這一點也是較為之後的事了。

「為什麼現在要說這個？」
「現在，我感覺很平靜。沒有生氣，也沒有悲傷。但是，只留下白詰殺了團十郎而產生的殺意。然後我會滿足那個殺意的吧，我能夠確信。」

桂確信著勝利。
那是當然的吧，我不認為這個情況下我還能取勝。

「不會有完成復仇的喜悅，有的只是從我心中，對於白詰的殺意消失了。」
「差不多，能回答我的問題了嗎？」

我語氣變得些許不耐煩。
只是說出意味深長的話，卻並沒有回答我『為什麼會說這樣的話』的問題。

「『不明白』────這就是我的答案。因為感情這些不必要的東西和人類的身體一起扔掉了，所以我殺了白詰，完成了廣瀨的復仇也不會高興。但是，到了要殺你的時候，不知為何我想和你交談。衝動地、情感性地。啊，也許是像感情殘渣一樣的東西在腦子的角落裡殘留了下來，推動了我進行交談嗎？」

總之，沒有完全捨棄感情嗎。
然後，這次為了捨棄，想去除掉最後的殘渣。

「為了消除那個情感，才和我交談嗎？」
「不能斷言，但是可以這樣想。也許是最接近的可能性。」

真是歪曲的說法呢。
但是如果能互相交談的話再好不過了。
與其要殺掉完全捨棄人性的桂，還是殺死有點人性的桂，更有復仇的價值。

「即使是恭維也不能說外表好看，身高也低，頭腦也不好，毫無精神，你姐姐和楠會對你愛慕是奇跡般的事。因為你沒有任何的魅力。」
「我不否定！」

雖然世界上所有人都討厭我，但比誰都討厭自己的是我。
我討厭什麼都做不到、無力、無能的我。

「在理解了不能自己解決事態的情況後，我有時會期待別人的幫助。比如，像桂那樣的人行動的話，班級裡的大家也都會聽的嗎？」

其實，越是討厭被別人背叛，就會對只能依賴別人的自己產生討厭的情緒。

「確實，如果解決了班級裡發生的欺凌的話，我的社會評價也會受到提高吧。但是，這是在教師和大人們的認可下才成立的。白詰的情況下，周圍的大人都是幫助加害者一方的。」

對了，大人們─────就連雙親都不是我的同伴。

「也就是說，即使我介入，欺凌解決了，大人們也不會評價我。反而會被貼上『充滿正義感的麻煩傢伙』的標簽。沒有任何好處。」

因為沒有利益，不會去救。
是個非常空虛的事實。
但是，也許這才是事物的本質。
如果說我家不是普通的家庭，而是大富大貴的人家，水木也會來幫我了吧。
當然的，是想要回報。

「但是我也是人，有時也會把好處扔到一邊，任由好奇心而去採取行動。」
「意思是你想救我嗎？」
「不啊，正相反！」

桂淡然地宣告著。
我所不知道的，也沒有必要知道的隱藏事實。

「楠從以前開始就對水木先生產生了警惕。也意識到他那猶如評定的視線。」
「⋯⋯為什麼這裡要提到彩花的名字呢？」

我根本不知道彩花和桂之間有所聯繫。

「要讓水木老師和楠兩人獨處的話，在某種程度上，信賴的第三者是必不可少的。」
「桂⋯⋯難道⋯⋯」

我的聲音顫抖了。
水木和彩花兩人獨處，這意味著────

「是的，我把楠引到了水木老師等待的房間。當然，在房間外，我也聽到了她拚命地哭喊著白詰的名字。楠還真是從心底裏信賴著白詰的啊，明明沒有任何魅力和利益，真是不可思議呢。」
「⋯⋯啊，桂，啊啊啊！」

任由怒氣衝上頭頂，釋放出甘狄拔。
瞄準的是眉間，即使知道不起作用也不得不做。
但是，赫洛斯只是輕輕地彎曲了脖子，就避開了射出的箭。

「在底層的白詰，有著兩個支柱。那麼，在那個消失的時候，在底層喘息的白詰，究竟會墮落到何種地步呢？我保持著這樣的興趣。」
「因為興趣，就因為興趣把彩花給⋯⋯！」
「彼此彼此吧，白詰也僅憑感情殺了團十郎！」
「所以怎麼了⋯⋯仇恨是不會相互抵消的，就算有著廣瀨的死，你所做的事實也是不會改變的！殺，殺，殺了你！」
「仇恨是不會抵消的，嗎？真理呢，對於沒有學問的白詰真是一句名言。但是這場決鬥是我的勝利，白詰死了────那是已經決定的結果。僅僅靠感情，無法填補壓倒性的實力差。」

最後的溝通結束了，從赫洛斯的雙手浮起了綠色的結晶體──散落。
綠色的碎片閃閃發光，傾瀉在森林裡。

「這裡周圍，到底棲息著多少生物呢？在地上爬行的野獸也好，昆虫也好，當然鳥類，我們都互相分享著。大家都在謳歌著，奧里哈魯鋼是一種很棒的物質。如果溝通的話，大家都是伙伴，也就是說，所有的，都是你的敵人。」

咋咋咋咋咋咋咋！

森林涌動，大地搖晃。
由於過度的衝擊，我不禁用手臂護住了頭部。
然後，在下一個瞬間，我的視野⋯⋯被魔物填埋了。
右面、左面、前面、後面、上面、看到的景色全部都是魔物、魔物、魔物。
像狗一樣的魔物露出了牙齒，甲虫的魔物用角朝著我，鳥的魔物帶著鈎爪。
除此之外，只有數一數就會頭暈的數量、各種類的魔物們一齊向我露出敵意。

「那，死吧，白詰？」

相對的，只有一隻瀕死的阿尼瑪。
情況是絶望的。
但是，我沒有想死的打算。
無論如何，也要跑掉。
活著，活下去，那種堅強的意志讓我振奮著。

還有留戀（姐姐）

還有約定（戀人）

還有復仇（殺了桂）

──不能死的理由太多了。

我拔出了劍，展示我的意志。
沒有老老實實等死的打算。
這成為了宣告戰鬥開幕的信號。
驅動鐵一樣的身體，在灰色的大地上蠢蠢欲動，然後─────魔物們行動了。

「噢噢噢噢噢噢噢噢！」

我發出了高喊，主動衝進了魔物形成的大軍中