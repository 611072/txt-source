秘密組織『黑貓』

這是一個被稱為黑貓的組織。
主要從事貨物的運輸工作，他們如同黑色的貓一般迅捷，穿梭于大街小巷之間，為各個國家運輸貨物並有著相當的信譽。

與其說是組織，不如說是商會更好一些吧，之所以稱為組織是因為他們的首領是一個謎一樣的人物。
雖然接手眾多工作，但是組織的首領卻很少在人前出現，組織的很多幹部也鮮為人知。

然而奇怪的是他們在大陸中情報收集工作卻做得比其他都好。

───

就是被稱為這個公會最高幹部的六人，聚集在位於大陸西南部的大國雷姆多利亞的首都的商會總部。

「那麼，第３４９６回最高首腦會議！開始咯！」

有著長髮及腰的緋紅色頭髮的少女任性的大聲呼喊到。

「首先要說的是關於消滅帝都所使用的魔法目前仍舊一無所知，還在繼續研究中。還有就是，這一代的勇者君，笨蛋的吉庫被保護了起來！就這樣！」

吉庫菲德？這次的勇者是男的吧？他還要靠女人來保護啊？靠著牆的大漢提問到。

「還在調查中。只是，那些人好像正向著伊斯特莉雅移動。」
「那麼下一個到我了。伊斯特莉雅好不容易是王子派那方的勝利。不過、雖然以作為王女的姐姐作為人質、但是成效還是不怎麼樣啊。」

說著這番話的、是有著平凡容貌的魔法使。雖然平凡但是卻有著老練的氣氛、手上拿著巨大且形狀複雜的法杖。在伊斯特莉雅附近、負責那個區域的褐色肌膚青年目光不知看向何處。

「魔族領地好像還很安靜。目前並沒有想要侵略的意思。但也有有可能在不斷積蓄力量的樣子。」

唉，眾人嘆了口氣。在這之後大漢又舉起手說道

「南西部已經不行了。露亞布拉已經是風中殘燭了。」

這問題上次會議也提出過了，在帝國已經消滅了的情況下，露亞布拉的血統和權威已經不存在了。幸好有借此迅速崛起的國家、那個地區的方針也因此成形了。現在只要暗中支援就好。

「西北部方面，科爾多巴就此對於擴張領土好像有明顯的動作，為了暗中殺竜而進行各種勸誘、但都被不屑地拒絶了」

一副一本正經的樣子的男裝少女報告到

然後五個人的視線集中在了坐在圓桌邊上的一個男人身上
他就是秘密組織黑貓的首領。
他張開口沉重的說道

「方隣王停止了對東方國家的入侵，轉而好像打算攻入大陸中央。」

───

雷姆多利亞王，方隣。在他的治理下，不僅成就了大國雷姆多利亞王國，還是其歷史上擁有國土最大的傑出國王

其對領土的慾望、長年來東方的七都市連合有了改變、其方針有了大轉變。
在失去首都的帝國這樣的情況下。雷姆多利亞王肯定有想要佔領帝國領土的野心。又或者說是宏大的理想。

對於失去了帝國的人類一方來說，為了度過千年紀而成為世界的中心是很有必要的。
雷姆多利亞王方隣毫無疑問是有著如此的器量，這在場的眾人都是這麼認為的了。

現在唯一的問題就是，他的年紀太大了，關於後繼者會引出很多問題
大王子已經參與國家內政，他卓越的能力不僅僅是說說罷了，實際上也是有著超過他英雄般的父親的評價。
問題是、年紀最小的王子。如果只是無能和暴虐的話、也就算了。但問題、據說這位小王子也有能的過頭了。

突破了他的父王長年來都沒成功攻陷過的七都市連合、成功攻進了那個領域。
聯合慌慌張張的請求和解、王也接受了。因為後勤補給的問題，因此判斷無法完全征服七國連合。
能幹的大王子和能幹的小王子。雖然兩人的關係本來是很好的，但是如果方隣王死去的話會怎樣呢，這樣的問題想起來就頭痛。

「雷姆多利亞的內部問題麼，那麼繼續這樣子觀察下去吧」

商會長繼續說道。
好像靈魂被磨滅似的、雖然這聲音聽起來年輕，但卻有氣無力的樣子。

關於這方面的談話告一段落了，正在這時，男裝少女舉起了手。

「突破暗黑迷宮的人出現了，有好好調查嗎？」

是個年輕的女孩，並且隊員也是年輕有為的傑出人士。正確的說，唯一的一點就是外表和年齡一致罷了。
正因為如此，引起了社會各界的關注，對此抱怨的人也有很多。尤其是關於殺竜的呼籲聲，都是來自她一個人的決定。
黑貓組織的能力姑且不論，但也想要一些突出的能力的人才，而對於此，這個女孩來說再好不過了。

「特別要注意的是，如果與科爾多巴有關的話，有必要探查她們的動向」

會長是這麼認為的。但是這對這種情報的細微差別，因此和魔王成為了決定性的差別⋯

───

會議就這樣結束了。即便是還不到一杯茶的時間，但這也是習以為常的事。

「那麼就這樣吧」

褐色皮膚的青年，大賢者亞傑魯佛特的身影消失了。

「下次再見」

相貌平平的魔法師，隨意的使用著大陸上還不為人知的轉移魔法

「真是麻煩啊。那麼下次再見啦」

緋色的頭髮的少女也是，使用轉移魔法離去。

「那麼會長，要多注意身體啊」

嚴肅的點頭致敬後，男裝少女也消失了。
只剩的大漢和把手肘撐在桌子上說，雙手交叉在嘴邊遮住嘴的會長。

「吶啊、大和」

大漢呼喚著會長。

「你那個樣子，差不多也該停止了吧？」
「為何？我向你這種偉大的人採取這種姿態是應該的吧」

是的，這樣的話就是對我說的，說的就是自己。
是的，當初是自己提出這樣的要求的。

真是愚蠢阿，年輕時期的自己實在太過輕率了。

「那麼差不多了，我也該回去了」
「你也差不多該學學轉移魔法了吧？」
「大和啊。你就饒了我吧」

揮了揮手後，大漢從大門走了出去。
在這之後只留下黑貓會長大和一人了

「這世界到底會怎麼樣呢？」

像老人一樣自言自語到，他站起來。

實際上，他老了。並不是指肉體上的，而是精神上的，越來越接近自己的極限了，但是不能因此放棄任務。
透過玻璃窗仰望天空，在天空下的一隅，是王城巨大的身影

───

戰錘和刀相互交錯

激烈的打鬥中，也有不乏讓人賞心悅目你來我往的攻防之戰
在一聲劇烈的武器交錯聲後，兩人彼此拉開了一段距離

「感覺如何⋯⋯」

將戰錘豎著放置在身旁，奧加王喘著粗氣說道。

「雖然氣勢動作比起原來好多了。但是，該怎麼說呢，總覺得好像技藝生疏了呢」

心裡明白但是不知道該怎麼具體的表達出來
對莉雅來說，這就足夠了。感覺也是生活必不可少的一部分

迷宮城市出發的莉雅一行人，現在在奧加的村子裡。
由於奧加王的召集，將各奧加村的村長們聚集了起來。現在正在等待著他們的到來。

莉雅為了充分利用那段時間，繼續和奧加王進行訓練。只是武器的對打的話將伊琳娜作為對手也是可以的，但如果將這技巧靈活的運用到實際戰鬥中，她還有著明顯的不足。

「雖然有點麻煩，但還是希望你能繼續作為我的對手」
「啊，那倒是沒什麼關係的，或者是一點也不介意。」

奧加是喜好戰鬥的種族。
他們一族，每天除了必要的修行以外，其他時間都過著自由自在的生活。
奧加們一直在等的是來自於魔族的連絡

───

在那個夜晚─。

在沒有事先約定的情況下，魔族的大幹部明日香突然出現在莉雅一行人的面前。
在她洩漏更多消息前，零搶先一步用鎖喉術阻止了她的發言。只有詳細知道整件事情的人才允許知道這些消息。

即，莉雅，薩基，零，明日香這４人。

「但是在那之前，明日香小姐」

薩基抬頭問道

「我允許你說話了，說吧，少年」
「謝謝。那麼你和零桑的名字，都是由魔王大人決定的嗎？」
「你知道的還真清楚呢。」
「那個，魔王在我前世的世界裡是出現於故事中的人物。」
「啊！原來如此，是這樣啊！」

明日香似乎對眼下提到的事情感到很熟悉。

說起來魔族領地不光擁有印刷技術，連漫畫都有。
聽了那番話之後薩基也相當認真的考慮過移居魔族領地的是，不過這是題外話了。

⋯魔王大人，文化汚染也要適可而止。

「總之，能將有關科爾多巴的情報告訴我們嘛？」

莉雅的提問，明日香也點頭表示認同。了解了大家的意思的零也輕輕點了點頭。

理由很簡單啊，敵人的敵人就是朋友，就是這樣。
對於魔族來說，人類是敵人，但並不是每個人類都是是敵人。在魔族的領地裡也有人類。
對魔族而言所謂的敵人，就是抹殺、排斥魔族的人。
具體地說，就是奉行人類至上主義的國家科爾多巴。

如果莉雅等人與科爾多巴敵對的話，她們會毫不吝惜的借出力量。

「藉由我的魔法，即使是遠距離交談也很簡單。」

雖然零多次嘗試讓明日香閉嘴，但她仍滔滔不絶的說著自己的能力。

這個少女該不會具有「低難度女主角」的特性吧。
雖然很沒禮貌，但是薩基不由得這樣想著。

事情都談的差不多了
雙方的意見差不多都統一了，也該是告別的時候了。
明日香兩眼放光的盯著莉雅說道

「血，莉雅你讓我吸點血怎麼樣？」
「請容我拒絶」
「別這樣說嘛，一點點就好，稍微刺一下而已。」

在這個情況下，所謂的稍微刺一下指的是用牙齒咬一下的意思。

「我吸血的方式很舒服的，那些被我吸的人都覺得爽呢，大家都這麼覺得呢。」
「被你吸血的話，我會被變成你的眷屬麼？」
「啊，沒問題沒事的啦。那是沒有魅惑耐性和疾病耐性的人才會發生的。而且我本人也沒這個意思所以沒問題的。」
「那麼⋯⋯作為成功後的酬勞如何？」
「呵呵」

話題朝著「如果能獲得有用的情報，就給予她少量鮮血」這個方向進行了。

聽到這件事的之後，零一副困擾的樣子，薩基則是一副快要萌死了的樣子。
人類一方裡也有各種各樣的人們在好好行動著喲，這樣的話。