芙拉朵・娜・波魯克庫拉特是一位瀟灑的淑女，比起卡麗婭・帕多利克等曾經的旅途中的其他成員，她的品格更加高尚。

她無比崇尚才智，熱愛著學識。
雖然有時會跟人產生隔閡，並不與他人隨意親近，儘管如此她也有著古板教條之處。
她與高嶺之花這個詞十分相稱。

我不清楚是不是她的母国博魯瓦特王朝里那裡的魔法師都如她這樣，但至少我在曾經的旅途中對芙拉朵這個人的認識，對她產生出這才是魔法師啊等如此這般的憧憬。

在我的印象裡，她本是個從不給人可趁之機的女人。

而如今卻截然不同，她杵在床上一動不動。
不但呆滯的瞳孔失去了焦點，嘴角也沒有像平時一樣緊繃起來。

她每次睡醒起來時，都是這副模樣。
我伸出手想幫她清醒過來，

「⋯⋯我還是睏，繼續睡了」

芙拉朵說完便揮開我的手，繼續躺倒在床上。她的表情是多麼安詳和舒暢。

真羨慕死我了，我也想像她這樣爽快的入眠。
我從嘴裡深深地呼出了發自肺腑的一口嘆息之氣。

在屋內配備的椅子上坐下來，椅子發出了碾軋的聲音，但就憑這點噪音公主殿下可醒不過來。
就算是在她耳邊大喊大叫，能否令她徹底清醒也得打個問號。
看向木製窗戶那邊，太陽已經高高升起，在天空中閃著光輝。

貧民窟這裡的居民大多都在夜晚出門，所以現在白天外面沒什麼動靜。
這裡真是無比的平靜。

歡睡的芙拉朵與天上高高的太陽。

自從搬到貧民窟以來，我已經司空見慣了這幅景象。
據芙拉朵所言，她是因無論如何也受不了晚上那些娼婦嬌艷的喘息聲而沒休息好。

我當然也同樣認為那聲音疹人，也因此而睡不好。
但先不說這個，這女人的起床狀況也太糟糕了。
就像今天這樣，我才把她喊起來，回頭卻又倒在床上。
嘴上說著起來了讓我剛放下心來，不用多久又開始了呼呼的睡。總是如此地折騰。
更可惡的是，她本人卻不記得這些賴床的事情。

而即使是她總算是從床上起來了後，卻還要悠然自如的搖動她那雙黑眼再發上個十來分鐘的呆。
看到這副景象，我絶對無法把她和普經那個毫無可趁之機的女人視為同一人物。甚至我會覺得她只不過是名字和相貌都相同的另一個人罷了。

當然也有可能是在普經的旅途中，除了我以外其他人也曾知曉她這副模樣，但我覺得那會兒也不至於糟糕到如此地步吧。

比如在陌生男人面前，展現自己毫無任何防備的姿態。
我輕輕拍了下芙拉朵的臉蛋，準備再一次讓她清醒。

看得出她那緊閉的眼皮顯露出一副厭煩的模樣。
揮動手指想要趕跑妨礙她繼續睡覺的我。

其實既然發睏的話讓她繼續睡下去倒也沒什麼，但今天可是她要讓我非喊她起來不可的。
既然和她約好了，那我也不能對她棄之不顧。

於是，這之後又上演了多次蠢到家的了我與芙拉朵之間的手指攻防戰。
芙拉朵帶著一股子睡意眨著眼，打了一個哈欠。

看來公主殿下總算是打算要起床了。芙拉朵抬起上半身搔著皮膚，但目光還有些呆滯。用著毫無氣力的腔調對我說，

「路易斯一⋯⋯給我拿熱湯來」聽到她的話我點了幾下頭以示回應。

我清楚的很她那聲音的目的是什麼，或者說是她之前令我搞明白了。

「好的好的，我明白了公主殿下，還是要喝那個啊。」

我從用小火燒著的爐中取出鍋來，將裡面的熱湯舀至陶碗中。鍋裡的只不過是藥草與開水混合的產物，但用來當作使人清醒的飲料是再合適不過了。

說實話我是不怎麼喜歡它那股會殘留在喉嚨的苦味，但總比沒味道的白開水要強，沒錢的時候我普經常喝它。

陶碗裡只盛了一半左右的分量，讓芙拉朵用雙手捧住。
曾經偶爾發生過因發呆而將熱湯潑灑在床上，所以我反覆確認了芙拉朵是把陶碗抓牢了，才將我的手輕輕鬆開。

芙拉朵慢悠悠的把碗貼_上嘴唇，喝下熱湯。
從她的動作來看，確實是個有教養的人。

跟我與伍德這樣的粗野之人喝水的動作完全不同。

在漫長的喝光熱湯之後，她那雙呆滯的黑眼總算取回了清醒的狀態。據她本人所述，直到這會兒基本還是在夢裡呢。

饒了我吧。

「唔⋯⋯早上好，路易斯，今天算是把我給喊起來了啊，嗯，這樣才對嘛。」

這早上真好啊，滿面笑容的芙拉朵說到。

撕裂我的嘴也不會告訴她，才不是早上啊，都中午了。
芙拉朵要整理裝束打扮，我從房間裡出去站在門外等著。

本打算一大早就把貧民窟的事辦完的，按這個情形，估計要到傍晚才能搞完了。
一邊我喝著盛給自己的熱湯，一邊眨眼思考。

擴散到滿嘴的味道真苦。這樣的東西真虧她能喝下去啊。

「要是你睏的話，就沒必要非要跟我一起去的，真不是什麼大不了的事情。」

不過就是跟貧民窟這邊的幾個有威望的人打個照面罷了。

事是要做，但沒必要讓芙拉朵出馬。
不如說外国人的她要是惹人注目了反而會變得麻煩。

所以我用這種隨便的口氣跟她說。
但是芙拉朵卻看的很重。

萬分尖銳的聲音從室內刺了過來。

「是嗎，之前你也是這麼說的，整整一天都沒有回來。
奇怪，是我記錯了嗎，還是說有人對同犯的我撒了謊呢？」

我想所謂話裡有毒就是如此吧。
芙拉朵話語與其說是毒物，更應該說是每個詞都帶著刺。
我可不會對此發表反駁。
嘴裡含著湯，含糊著應付過去。

又過了一會兒。

不清楚是因為芙拉朵出身名門，還是說她個人速度就是如此，總之她的收拾打扮非常花時間。
我聽說魔法師的事先準備確實花時間，搞不好也是受了這個影響吧。

突然間，我喜梢眉頭。
嘴裡咬著嚼煙，我隔著門板向她問到。

「說起來，你還沒習慣晚上嗎，雖然我也懂你的心情啦。」

問完後，一時間對面傳來唔的一聲。
看來是在考慮怎麼回答我吧。

「那可不是什麼習慣不習慣的事情，只不過是稍微好轉了一點吧，就一點，現在總算是睡著了。」

芙拉朵的話語似乎聽著就像負有愧疚的事情一樣，難以開口。
當然了，像芙拉朵這樣的人與娼館毫無緣分。我也不難理解她會對此感到困惑。
可突然又一想，既然已經能睡著了，那為什麼起床還這麼晚呢。
說起來是她本來早上就難起床吧。

還是說，她口中的習慣了，其實並不是真心話嗎。
我向她投出的這些問題，一下子就被消滅了。
芙拉朵帶著有些不滿的樣子，繼續說到，

「還不是因為⋯⋯某人每次回來都很晚啊，平安歸來還好，可有個萬一我卻因為先睡了而錯過，豈不是蠢死了。」

聽完這話，我被咽下的藥湯從口中逆流而上而嗆到，努力調整好呼吸。
確實我晚上無規律出門的情況比較多。
因為對我而言，晚上才方便行動。
怎麼著，芙拉朵是每次我出門都特意為我醒著，等到確認我回來後她才入睡的嗎。
我不由得按住太陽穴，陷入沉思。

啊，對了。

芙拉朵・娜・波魯克庫拉特這個人就是有些奇怪的古板之處。
於是這些古板自然而然地也同樣對同犯的我所適用。

小聲的發出了一聲芙拉朵她聽不見的嘆息。
現在這種心情還真是未普出現過。
至少在普經的旅途中未普遇到這樣的經歷，所以說實在的我不直到如何應對。

一時間，我繼續沉默下去。

也不知道過了多久。
但我知道至少是芙拉朵完成了她的收拾打扮吧。

芙拉朵束好頭髮並整理好了衣裝，一邊問到你怎麼了，一邊推開門向外面看。
從她鎮定自若的表情上看不出任何陰影。

我不由得聳了聳肩，說到，

「什麼事情都沒有，公主殿下。咱們出門吧，盡量趕在公主殿下睡覺的時間前回來吧。」

我的話語充滿了傻氣，可內心被羞愧所佔據，也只能說出如此的話語來。
接著便牽起了芙拉朵的手來。

芙拉朵一時間有些不解，可還是點頭答應了。

「嗯，我對你的承諾表示感謝，共犯閣下。」